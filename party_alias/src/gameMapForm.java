/**
 * Created by Karol on 2016-01-15.
 */
public class gameMapForm extends JFrame {
    private JList list1;
    private JPanel panel;
    private JPanel panel2;
    private Canvas canvas;

    public gameMapForm() {
        super("friends");
        setContentPane(panel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);


        panel2.add(new myCanvas());
    }

    class myCanvas extends Canvas {
        public myCanvas() {
            setBackground(Color.CYAN);
            setSize(500, 500);
        }

        public void paint(Graphics g) {
            Graphics2D g2;
            g2 = (Graphics2D) g;
            g2.drawString("It is a custom canvas area", 70, 70);
        }
    }

}