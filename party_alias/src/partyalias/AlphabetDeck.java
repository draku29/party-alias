package partyalias;

import java.util.List;

public class AlphabetDeck extends Deck {

    public AlphabetDeck(List<AlphabetCard> cards) {
        this.setCards(cards);
    }
}