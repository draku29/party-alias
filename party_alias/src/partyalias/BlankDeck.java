package partyalias;

import java.util.List;

/**
 * Created by Karol on 2016-01-24.
 */
public class BlankDeck extends Deck {
    public BlankDeck(List<BlankCard> cards) {
        this.setCards(cards);
    }
}
