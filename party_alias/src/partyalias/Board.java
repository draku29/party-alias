package partyalias;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Board extends JPanel{
	private static final long serialVersionUID = 1L;
	private BufferedImage backgroundBoard;
	private int[] fieldCoordsX= {513, 472, 431, 390, 349, 308, 267, 226, 185, 144, 103, 62, 21, 21, 21, 21, 21, 21, 21, 21, 35, 77, 103, 103, 103, 103, 103, 103, 133, 173, 219, 259, 299, 339, 379, 421, 450, 451, 451, 451, 451, 451, 451, 451, 430, 391, 347, 304, 264, 221, 188, 190, 228, 269, 310, 351, 392, 433, 474, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512};
	private int[] fieldCoordsY= {498, 498, 498, 498, 498, 498, 498, 498, 498, 498, 498, 498, 498, 458, 418, 378, 338, 298, 258, 218, 180, 177, 208, 248, 288, 328, 368, 408, 435, 442, 444, 444, 444, 444, 444, 440, 412, 371, 331, 291, 251, 211, 171, 131, 97, 98, 97, 97, 98, 96, 72, 30, 20, 22, 22, 22, 22, 22, 22, 24, 64, 104, 144, 184, 224, 264, 304, 344, 384, 424, 464};
	private FieldTypes[] types= {FieldTypes.START_FINISH, FieldTypes.ALPHABET, FieldTypes.REGULAR, FieldTypes.PANTOMIME, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.MULTICOLOR, FieldTypes.VIP, FieldTypes.PANTOMIME, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.PANTOMIME, FieldTypes.RHYME, FieldTypes.VIP, FieldTypes.ALPHABET, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.PANTOMIME, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.PANTOMIME, FieldTypes.RHYME, FieldTypes.MULTICOLOR, FieldTypes.MULTICOLOR, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.ALPHABET, FieldTypes.PANTOMIME, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.ALPHABET, FieldTypes.MULTICOLOR, FieldTypes.RHYME, FieldTypes.VIP, FieldTypes.PANTOMIME, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.MULTICOLOR, FieldTypes.RHYME, FieldTypes.PANTOMIME, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.REGULAR, FieldTypes.ALPHABET, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.VIP, FieldTypes.REGULAR, FieldTypes.RHYME, FieldTypes.MULTICOLOR, FieldTypes.PANTOMIME, FieldTypes.MULTICOLOR, FieldTypes.MULTICOLOR, FieldTypes.REGULAR, FieldTypes.REGULAR};
	private Field[] fields= new Field[71];
	private Pawn[] pawns= new Pawn[2];
	private Roulette roulette;

	public Board(){
		loadImages();
		initializeFields();
		repaint();
		pawns[0]= new Pawn();
		pawns[1]= new Pawn();
		pawns[0].move(70); //test funkcji move
		roulette= new Roulette(this);
		repaint();
		roulette.spin();
	}

	private void initializeFields(){
		for(int i=0; i<51; i++){
			fields[i]= new Field();
			fields[i].setNumber(i);
			fields[i].setXCoord(fieldCoordsX[i]);
			fields[i].setYCoord(fieldCoordsY[i]);
			fields[i].setType(types[i]);
		}
	}

	private void loadImages(){
		String imgName= "board.jpg";
		URL imgURL= getClass().getResource("/"+imgName);
		try {
				backgroundBoard = ImageIO.read(imgURL);
		} catch (IOException e) {
			System.err.println("Blad odczytu obrazka");
			e.printStackTrace();
		}

	}

	public void render_Board(Graphics2D g2d) {
		g2d.drawImage(backgroundBoard, 0, 0, this);
	}

	public void render_Pawns(Graphics2D g2d) {
		g2d.setColor(Color.blue);
		g2d.fillOval(fieldCoordsX[pawns[0].getFieldNumber()]-7, fieldCoordsY[pawns[0].getFieldNumber()]-7, 20 ,20 );
		g2d.setColor(Color.red);
		g2d.fillOval(fieldCoordsX[pawns[1].getFieldNumber()]+7, fieldCoordsY[pawns[1].getFieldNumber()]+7, 20 ,20 );
	}

	public void render_Dice() {
		// TODO - implement Board.render_Dice
		throw new UnsupportedOperationException();
	}

	public void render_HourGlass() {
		// TODO - implement Board.render_HourGlass
		throw new UnsupportedOperationException();
	}

	public void render_Gui() {
		// TODO - implement Board.render_Gui
		throw new UnsupportedOperationException();
	}
	public void render_Roulette(Graphics2D g2d,int x, int y) {
		g2d.setColor(new Color(0, 255, 0, 50));
		g2d.fillOval(x, y, 50 ,50 );
	}

	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		render_Board(g2d);
		render_Pawns(g2d);
		render_Roulette(g2d, roulette.getActX(), roulette.getActY());


	}
}
