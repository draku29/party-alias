package partyalias;

public abstract class Card {

    /**
     * 8 item array of String
     */
    private String[] words;
    private boolean isDrawn;

    /**
     *
     * @return String[]
     */
    public String[] getWords() {
        return this.words;
    }

    /**
     * @param position
     *
     * @return String
     */
    public String getWord(int position) {
        return this.words[position];
    }

    /**
     *
     * @return boolean
     */
    public boolean getIsDrawn() {
        return this.isDrawn;
    }

    /**
     * @param words
     */
    public void setWords(String[] words) {
        this.words = words;
    }

    /**
     * @param word
     * @param position
     */
    public void setWord(String word, int position) {
        this.words[position] = word;
    }

    /**
     * @param isDrawn
     */
    public void setIsDrawn(boolean isDrawn) {
        this.isDrawn = isDrawn;
    }

}