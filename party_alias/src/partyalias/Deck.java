package partyalias;

import partyalias.Card;

import java.util.List;
import java.util.Stack;
import java.util.Collections;

public abstract class Deck {

    private List<? extends Card> cards;
    private Stack<Card> stack;

    public List<? extends Card> getCards() {
        return this.cards;
    }

    /**
     * @param cards
     */
    public void setCards(List<? extends Card> cards) {
        this.cards = cards;
    }

    public Stack<Card> getStack() {
        return this.stack;
    }

    public void createStack() {
        stack.addAll(cards);
        Collections.shuffle(stack);
    }

    /**
     * @param number
     */
    public Card[] draw(int number) {
        Card[] substack = new Card[number];
        for(int i=0; i< number; i++){
            if(!stack.empty()) {
                substack[i] = stack.pop();
            } else {
                createStack();
            }
        }
        return substack;
    }

}