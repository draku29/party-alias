package partyalias;

public class Decks {
    public RegularDeck regularDeck;
    public RhymeDeck rhymeDeck;
    public PantomimeDeck pantomimeDeck;
    public VIPDeck vipDeck;
    public AlphabetDeck alphabetDeck;
    public BlankDeck blankDeck;
}