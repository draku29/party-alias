package partyalias;

public class Field {

    private FieldTypes type;
    private int color;
    private int number;
    private int xCoord;
    private int yCoord;
    private int id;

    public int getColor() {
        throw new UnsupportedOperationException();
        return this.color;
    }

    public FieldTypes getType() {
        return this.type;
    }

    /**
     * @param type
     */
    public void setType(FieldTypes type) {
        this.type = type;
    }

    public int getNumber() {
        throw new UnsupportedOperationException();
        return this.number;
    }

    /**
     * @param number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        throw new UnsupportedOperationException();
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

}