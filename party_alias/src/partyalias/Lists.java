package partyalias;
import java.util.LinkedList;

/**
 * Created by Karol on 2016-01-23.
 */
public class Lists extends  RandomCharacterGenerator{
    RandomCharacterGenerator rcg;

    public static LinkedList<Field> fields = new LinkedList<Field>();
    public static LinkedList<AlphabetCard> aCards = new LinkedList<AlphabetCard>();
    public static LinkedList<PantomimeCard> pCards = new LinkedList<PantomimeCard>();
    public static LinkedList<RegularCard> regCards = new LinkedList<RegularCard>();
    public static LinkedList<RhymeCard> rhymeCards = new LinkedList<RhymeCard>();
    public static LinkedList<VIPCard> vipCards = new LinkedList<VIPCard>();
    public static LinkedList<BlankCard> blankCards = new LinkedList<BlankCard>();


    public Lists() {
        AlphabetCard aCard = new AlphabetCard();
        PantomimeCard pCard =new PantomimeCard();
        RegularCard regCard = new RegularCard();
        RhymeCard rhymeCard = new RhymeCard();
        VIPCard vipCard = new VIPCard();
        BlankCard blankCard = new BlankCard();

        for(int i=0;i<5;i++){
            aCard.setWords(rcg.RandomCharacterGenerator());
            aCards.add(aCard);
        }

        for(int i=0;i<34;i++){
            pCard.setWords(rcg.RandomWordGenerator());
            pCards.add(pCard);
        }

        for(int i=0;i<192;i++){
            regCard.setWords(rcg.RandomWordGenerator());
            regCards.add(regCard);
        }

        for(int i =0;i<75;i++){
            rhymeCard.setWords(rcg.RandomWordGenerator());
            rhymeCards.add(rhymeCard);
        }

        for(int i=0;i<92;i++){
            vipCard.setWords(rcg.RandomWordGenerator());
            vipCards.add(vipCard);
        }

        for(int i=0;i<5;i++){
            blankCard.setWord(rcg.blank,1);
        }

    }

}
