package partyalias;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class MainClass extends JFrame {
    private static final long serialVersionUID = 1L;
    private static int width = 800;
    private static int height = 570;
    private static int screen_width, screen_height;

    public MainClass() {
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {

                dispose();
                System.exit(0);
            }
        });
        screen_resolution();
        setSize(width, height);
        setResizable(false);
        setLocation((screen_width - width) / 2, (screen_height - height) / 2);
        setVisible(true);
        setTitle("PartyAlias");

    }

    private void screen_resolution() {
        Toolkit tools = Toolkit.getDefaultToolkit();
        Dimension dimension = tools.getScreenSize();
        screen_width = dimension.width;
        screen_height = dimension.height;

    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                @SuppressWarnings("unused")
                MainClass mainc = new MainClass();
                mainc.setContentPane(new Board());
            }
        });
    }
}
