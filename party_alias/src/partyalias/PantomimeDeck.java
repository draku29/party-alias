package partyalias;

import java.util.List;

public class PantomimeDeck extends Deck {
    public PantomimeDeck(List<PantomimeCard> cards) {
        this.setCards(cards);
    }
}