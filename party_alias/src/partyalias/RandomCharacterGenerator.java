package partyalias;

import java.util.Random;

/**
 * Created by Karol on 2016-01-24.
 */
public class RandomCharacterGenerator {
    Random r = new Random();
    int index,length;
    String blank;
    String alfabet = "abcdefghijklmnoprstuqwxyz";
    String words[] = new String[8];
    String wholeWords[] = new String[8];
    public String[] RandomCharacterGenerator(){
        for(int i=0;i<8;i++){
            index = r.nextInt(8);
            words[i] = alfabet.substring(index,index);
        }
        return words;
    }
    public String[] RandomWordGenerator(){
        for(int i=0;i<8;i++){
            index = r.nextInt(8);
            length = r.nextInt(3);
            wholeWords[i] = alfabet.substring(index,length);
        }
        return wholeWords;
    }
    public String BlankText(){
        blank = "Wypowiedz swoje zadanie !";
        return blank;
    }
}
