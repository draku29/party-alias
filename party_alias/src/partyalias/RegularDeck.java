package partyalias;

import java.util.List;

public class RegularDeck extends Deck {
    public RegularDeck(List<RegularCard> cards) {
        this.setCards(cards);
    }
}