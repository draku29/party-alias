package partyalias;

import java.util.List;

public class RhymeDeck extends Deck {
    public RhymeDeck(List<RhymeCard> cards) {
        this.setCards(cards);
    }
}